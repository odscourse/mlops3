<!-- omit in toc -->
# Contributing to MLOps 3 Student Project

First off, thanks for taking the time to contribute! ❤️

All types of contributions are encouraged and valued. See the [Table of Contents](#table-of-contents) for different ways to help and details about how this project handles them. Please make sure to read the relevant section before making your contribution. It will make it a lot easier for us maintainers and smooth out the experience for all involved. The community looks forward to your contributions. 🎉

<!-- omit in toc -->
## Table of Contents

- [I Have a Question](#i-have-a-question)
- [Using Ruff Linter in the Project](#using-ruff-linter-in-the-project)
  - [Installation](#installation)
  - [Running Ruff](#running-ruff)
  - [Configuration](#configuration)
- [Before Opening a Merge Request](#before-opening-a-merge-request)


## I Have a Question

Before you ask a question, it is best to search for existing [Issues](https://gitlab.com/odscourse/mlops3/issues) that might help you. In case you have found a suitable issue and still need clarification, you can write your question in this issue. It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/odscourse/mlops3/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions, depending on what seems relevant.

We will then take care of the issue as soon as possible.

## Using Ruff Linter in the Project

We use Ruff as a Python linter to maintain code quality and consistency in our project. Ruff analyzes your Python code based on rules defined in the `pyproject.toml` file.

### Installation

Before using Ruff, make sure you have it installed in your Python environment. You can install Ruff using pip:

```bash
pip install ruff
```

### Running Ruff
To run Ruff and analyze your Python code, navigate to the root directory of your project and execute the following command:

```bash
ruff check
```

### Configuration
The configuration for Ruff is defined in the `pyproject.toml` file located in the root directory of our project. This file contains rules and settings that Ruff uses to analyze your code. Make sure to review and understand these rules before contributing to the project. If you have any questions about specific rules or configurations, feel free to ask in the project's communication channels.

## Before Opening a Merge Request
Before opening a merge request, make sure to:

- Run Ruff to analyze your code and ensure compliance with the project's coding standards.
- Address any errors or warnings reported by Ruff.
- Follow any additional guidelines or requirements specified in the project's documentation.

Thank you for your cooperation!
